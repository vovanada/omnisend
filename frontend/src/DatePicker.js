import React from 'react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';

export default class DatePicker extends React.Component {
    constructor(props) {
        super(props);
        this.handleDayChange = this.handleDayChange.bind(this);
        this.state = {
            selectedDay: undefined,
            isDisabled: false,
        };
    }
    handleDayChange(selectedDay) {
        this.setState({
            selectedDay,
        });
        this.props.onSelect(selectedDay)
    }
    render() {
        const { selectedDay} = this.state;
        return (
            <div>
                <DayPickerInput
                    value={selectedDay}
                    onDayChange={this.handleDayChange}
                />
            </div>
        );
    }
}