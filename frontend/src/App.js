import React, {Component} from 'react';
import './App.css';
import DatePicker from "./DatePicker";

const API_URL = "http://localhost:1501";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            comments: [],
            firstDate: "",
            lastDate: "",
            isLoaded: false
        };
    }

    selectFirstDate(date) {
        this.setState({firstDate: date}, () => {
            this.reloadData();
        });
    }

    selectLastDate(date) {
        this.setState({lastDate: date}, () => {
            this.reloadData();
        });
    }

    reloadData() {
        if (this.state.lastDate !== "" && this.state.firstDate !== "") {
            this.loadData()
        }
    }

    loadData() {
        fetch(API_URL + "/reviews?first=" + this.parseDate(this.state.firstDate) +
            "&last=" + this.parseDate(this.state.lastDate))
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        comments: result
                    });
                }
            )
    }

    parseDate(date) {
        if (date === "") {
            return ""
        }

        return date.toISOString().substring(0, 10)
    }

    componentDidMount() {
        this.loadData()
    }

    render() {
        const {isLoaded, comments, firstDate, lastDate} = this.state;
        if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (

                <div className="App">
                    <header className="App-header">
                        <h1 className="App-title">Three-word phrases</h1>
                    </header>

                    <div className="App-intro">
                        Range from
                        <DatePicker date={firstDate} onSelect={this.selectFirstDate.bind(this)}/>
                        to
                        <DatePicker date={lastDate} onSelect={this.selectLastDate.bind(this)}/>
                        {comments !== null ?
                            <ul className="App-ui">
                                {comments.map(comment => (
                                    <li key={comment.text}>
                                        {comment.text} ({comment.total})
                                    </li>
                                ))}
                            </ul>
                            :
                            <p>Choose another date</p>
                        }
                    </div>
                </div>
            );
        }
    }
}

export default App;
