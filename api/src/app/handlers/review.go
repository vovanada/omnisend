package handlers

import (
	"gopkg.in/mgo.v2"
	"net/http"
	"app/models"
	"encoding/json"
	"log"
)

type Review struct {
	Database *mgo.Database
}

func (r *Review) Handler(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	first := req.URL.Query().Get("first")
	last := req.URL.Query().Get("last")

	phrases, err := models.GetTop5Phrases(r.Database, first, last)

	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	json.NewEncoder(w).Encode(phrases)
}
