package models

import (
	"time"
	"gopkg.in/mgo.v2"
	"fmt"
)

const CommentTable = "comments"

type Comment struct {
	Text    string
	Rate    string
	Created time.Time
}

func (c *Comment) Save(db *mgo.Database) error {
	threeWords := getThreeWordPhrases(c.Text)
	err := db.C("comments").Insert(c)
	if err != nil {
		return fmt.Errorf("error while saving comment, err: %s", err)
	}
	for _, p := range threeWords {
		err := db.C("phrases").Insert(
			Phrase{Text: p, Date: c.Created},
		)
		if err != nil {
			return fmt.Errorf("error while saving phrase, err: %s", err)
		}
	}
	return nil
}
