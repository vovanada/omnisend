package models

import (
	"time"
	"regexp"
	"log"
	"strings"
	"fmt"
	"gopkg.in/mgo.v2/bson"
	"gopkg.in/mgo.v2"
)

const PhrasesTable = "phrases"

type Phrase struct {
	Text string
	Date time.Time
}

type SearchPhrase struct {
	Text  string `bson:"_id" json:"text"`
	Total int    `bson:"total" json:"total"`
}

func GetTop5Phrases(db *mgo.Database, first, last string) ([]SearchPhrase, error) {
	var i []SearchPhrase

	var query []bson.M

	if first != "" && last != "" {
		f, err := time.Parse(time.RFC3339, first+"T00:00:00-05:00")
		if err != nil {
			return i, fmt.Errorf("error while parsing first date, err: %s", err)
		}
		l, err := time.Parse(time.RFC3339, last+"T23:59:59-05:00")
		if err != nil {
			return i, fmt.Errorf("error while parsing last date, err: %s", err)
		}

		query = append([]bson.M{
			{
				"$match": bson.M{
					"date": bson.M{
						"$gte": f,
						"$lte": l,
					},
				},
			},
		})
	}

	query = append(query, []bson.M{
		{"$group": bson.M{"_id": "$text", "total": bson.M{"$sum": 1}}},
		{"$sort": bson.M{"total": -1}},
		{"$limit": 5},
	}...)

	err := db.C(PhrasesTable).Pipe(query).All(&i)

	return i, err
}

func getThreeWordPhrases(s string) []string {
	var p []string

	reg, err := regexp.Compile("[^\\p{L}\\s]+")
	if err != nil {
		log.Fatal(err)
	}
	s = reg.ReplaceAllString(s, "")

	reg, err = regexp.Compile("\\s+")
	if err != nil {
		log.Fatal(err)
	}
	s = reg.ReplaceAllString(s, " ")

	s = strings.ToLower(s)

	words := strings.Split(s, " ")
	for i := 0; i < len(words)-2; i++ {
		p = append(p, fmt.Sprintf("%s %s %s", words[i], words[i+1], words[i+2]))
	}

	return p
}
