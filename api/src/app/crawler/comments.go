package crawler

import (
	"gopkg.in/mgo.v2"
	"github.com/PuerkitoBio/goquery"
	"fmt"
	"log"
	"time"
	"app/models"
)

const projectUrl = "https://apps.shopify.com/omnisend"

type Crawler struct {
	database *mgo.Database
}

func NewCrawler(database *mgo.Database) *Crawler {
	c := Crawler{database: database}
	return &c
}

func (c *Crawler) GetComments() chan models.Comment {
	ch := make(chan models.Comment)
	go func() {
		log.Println("saving comments")
		defer close(ch)

		p := 1
		for {
				log.Printf("working with page %v", p)
			doc, err := goquery.NewDocument(fmt.Sprintf("%s?page=%v", projectUrl, p))
			if err != nil {
				log.Println(err)
				return
			}

			doc.Find(".resourcesreviews-reviews-star").Each(func(i int, s *goquery.Selection) {
				var c models.Comment
				s.Find("blockquote").Each(func(j int, ss *goquery.Selection) {
					c.Text = ss.Text()
				})

				s.Find("meta").Each(func(j int, ss *goquery.Selection) {
					if v, ok := ss.Attr("itemprop"); ok && v == "reviewRating" {
						if r, ok := ss.Attr("content"); ok {
							c.Rate = r
						}
					}

					if v, ok := ss.Attr("itemprop"); ok && v == "datePublished" {
						if r, ok := ss.Attr("content"); ok {
							t, err := time.Parse(time.RFC3339, r)
							if err != nil {
								log.Printf("error while parsing time, err: %s", err)
							}
							c.Created = t
						}
					}

				})

				ch <- c
			})

			if doc.Find(".next_page").HasClass("disabled") {
				log.Println("saving comments is done")
				break
			}
			p++
		}
	}()

	return ch
}
