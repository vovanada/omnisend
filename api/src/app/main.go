package main

import (
	"net/http"
	"gopkg.in/mgo.v2"
	"log"
	"app/handlers"
	"fmt"
	"os"
	"app/data"
)

const Database  = "omnisend"

func main() {
	server := os.Getenv("OMNISEND_MONGODB")
	session, err := mgo.Dial(server)
	if err != nil {
		panic(fmt.Errorf("error while connect mongodb %s, err: %s", server, err))
	}
	defer session.Close()
	db := session.DB(Database)

	d := data.Data{Database: db}
	if d.IsTablesEmpty() {
		err := d.LoadComments()
		if err != nil {
			panic(err)
		}
	}

	review := handlers.Review{Database: db}

	http.HandleFunc("/reviews", review.Handler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
