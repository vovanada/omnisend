package data

import (
	"gopkg.in/mgo.v2"
	"app/crawler"
	"gopkg.in/mgo.v2/bson"
	"log"
	"app/models"
	"fmt"
)

type Data struct {
	Database *mgo.Database
}

func (d *Data) LoadComments() error {
	c := crawler.NewCrawler(d.Database)
	_, err := d.Database.C(models.CommentTable).RemoveAll(bson.M{})
	if err != nil {
		return fmt.Errorf("error while removing from table %s, error:%s", models.CommentTable, err)
	}
	_, err = d.Database.C(models.PhrasesTable).RemoveAll(bson.M{})
	if err != nil {
		return fmt.Errorf("error while removing from table %s, error:%s", models.CommentTable, err)
	}
	for comment := range c.GetComments() {
		err := comment.Save(d.Database)
		if err != nil {
			return err
		}
	}
	return nil
}

func (d *Data) IsTablesEmpty() bool {
	c, err := d.Database.C(models.CommentTable).Count()
	if err != nil {
		log.Printf("error while count comments, error: %s", err)
	}

	p, err := d.Database.C(models.PhrasesTable).Count()
	if err != nil {
		log.Printf("error while count comments, error: %s", err)
	}

	if p == 0 || c == 0 {
		return true
	}

	return false
}
